﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class PickupPoint_EndlessRunner : MonoBehaviour {
    public int score;
    private ScoreManager_EndlessRunner scoreManager;
    private AudioSource coinSound;

    // Use this for initialization
    void Start() {
        scoreManager = FindObjectOfType<ScoreManager_EndlessRunner>();
        coinSound = GameObject.Find("CoinSound").GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.name.Equals("Player")) {
            if (coinSound.isPlaying) {
                coinSound.Stop();
            }
            coinSound.Play();

            scoreManager.AddScore(score);
            gameObject.SetActive(false);
        }
    }
}