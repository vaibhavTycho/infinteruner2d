﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu_EndlessRunner : DeathMenu_EndlessRunner
{

    public GameObject pauseMenu;
  
    public void PauseGame() {
        // Time.timeScale = 0f;
        PlayerController_EndlessRunner.isPlayingNinja = false;
        pauseMenu.SetActive(true);
        transform.GetComponent<Image>().enabled = false;
    }

    public void ResumeGame() {
        PlayerController_EndlessRunner.isPlayingNinja = true;

        pauseMenu.SetActive(false);
        gameObject.SetActive(true);
        transform.GetComponent<Image>().enabled = true;

    }
    public void OnApplicationPause(bool pause)
    {
        Debug.Log("appPause "+pause);
        if(pause)
            PauseGame();
    }
    public void OnApplicationFocus(bool focus)
    {
        Debug.Log("focus "+focus);
        if (!focus)
            PauseGame();
    }
    public override void Retry() {
        ResumeGame();
        base.Retry();
    }

    public override void QuitToMainMenu() {
        ResumeGame();
        base.QuitToMainMenu();
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (Time.timeScale == 0)
                ResumeGame();
            else
                PauseGame();
        }
      
    }
}
