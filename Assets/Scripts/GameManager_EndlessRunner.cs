﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager_EndlessRunner : MonoBehaviour {

    public Transform platformGenerator;
    private Vector3 platformGeneratorStartPoint;

    public PlayerController_EndlessRunner playerController;
    private Vector3 playerStartPoint;
    [SerializeField] private Text deathScore;
    private ObjectDestroyer_EndlessRunner[] objectDestroyers;
    private ScoreManager_EndlessRunner scoreManager;
    public DeathMenu_EndlessRunner deathMenu;
  //  private PowerUpManager powerUpManager;
    [SerializeField] private PauseMenu_EndlessRunner pauseMenu;
    [SerializeField] private GameObject scoreGm;
    [SerializeField] private BgMove_EndlessRunner[] bgMove_EndlessRunners;
    public GameObject highscoreText;
  
    // Use this for initialization
    void Start () {
        platformGeneratorStartPoint = platformGenerator.position;
        playerStartPoint = playerController.transform.position;
      //  powerUpManager = FindObjectOfType<PowerUpManager>();
        scoreManager = FindObjectOfType<ScoreManager_EndlessRunner>();
    }

    public void RestartGame() {
        PlayerController_EndlessRunner.isPlayingNinja = false;
        pauseMenu.gameObject.SetActive(false);
        scoreGm.SetActive(false);
        scoreManager.scoreIncreasing = false;
        playerController.gameObject.SetActive(false);
        deathMenu.gameObject.SetActive(true);
        var tempscore = scoreManager.GetScore();
        deathScore.text = tempscore.ToString();
  
        if (PlayerPrefs.HasKey("HighScores"))
        {
            if (tempscore > PlayerPrefs.GetFloat("HighScores"))
                highscoreText.SetActive(true);
            else
                highscoreText.SetActive(false);

        }
    }

    public void ResetGame() {
        //  powerUpManager.InActivePowerUpMode();
      
        deathMenu.gameObject.SetActive(false);
        pauseMenu.gameObject.SetActive(true);
        scoreGm.SetActive(true);
        objectDestroyers = FindObjectsOfType<ObjectDestroyer_EndlessRunner>();
        for (int i = 0; i < objectDestroyers.Length; i++) {
            objectDestroyers[i].gameObject.SetActive(false);
        }

        playerController.transform.position = playerStartPoint;
        platformGenerator.position = platformGeneratorStartPoint;
        playerController.gameObject.SetActive(true);

        scoreManager.scoreCounts = 0;
        scoreManager.scoreIncreasing = true;
        for (int i = 0; i < bgMove_EndlessRunners.Length; i++)
        {
            bgMove_EndlessRunners[i].ResetGame();
        }
    }
}