﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu_EndlessRunner : MonoBehaviour {

    [SerializeField] private Text highScore;

    private void Start()
    {
       /* if (PlayerPrefs.HasKey("HighScores"))
        {
            highScore.text = PlayerPrefs.GetFloat("HighScores").ToString();
        }*/
    }
    public void StartGame() {
        PlayerController_EndlessRunner.isPlayingNinja = true;
        gameObject.SetActive(false);
    }

    public void QuitGame() {
        Application.Quit();
    }
}
