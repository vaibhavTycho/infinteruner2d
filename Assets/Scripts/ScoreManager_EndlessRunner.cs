﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager_EndlessRunner : MonoBehaviour {

    public Text scoreText, highScoreText;
    public float scoreCounts, highScoreCounts, pointPerSecond;
    public bool scoreIncreasing;
    public bool coinDoublePoints;
	
    // Use this for initialization
    void Start() {
       
        if (PlayerPrefs.HasKey("HighScores")) {
            highScoreCounts = PlayerPrefs.GetFloat("HighScores",0);
        }
        highScoreText.text = "High Score: " + Mathf.Round(highScoreCounts);
    }
	
    // Update is called once per frame
    void Update () {
        /*if (scoreIncreasing) {
            scoreCounts += pointPerSecond * Time.deltaTime;
        }*/

        if (scoreCounts > highScoreCounts) {
            highScoreCounts = scoreCounts;
            PlayerPrefs.SetFloat("HighScores", highScoreCounts);
        }
		
        scoreText.text = Mathf.Round(scoreCounts).ToString();
        highScoreText.text = "High Score: " + Mathf.Round(highScoreCounts);
    }

    public void AddScore(int point) {
        scoreCounts += coinDoublePoints ? point * 2 : point;
    }
    public float GetScore()
    {
        return scoreCounts;
    }
}