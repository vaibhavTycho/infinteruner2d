﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathMenu_EndlessRunner : MonoBehaviour {

    public string mainSceneName;
  
    public virtual void Retry() {
        // FindObjectOfType<GameManager_EndlessRunner>().ResetGame();
        SceneManager.LoadScene(0);
    }

    public virtual void QuitToMainMenu() {
        SceneManager.LoadScene(mainSceneName);
    }

}
