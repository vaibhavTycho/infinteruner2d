﻿using UnityEngine;
using System.Collections;

public class LandMove_EndlessRunner : ObjectMove_EndlessRunner
{

	[SerializeField] private float OffsetPosition;
	[SerializeField] private float xpos;
	private int firstIndex = 0;

	private void Update () {
		if (stateInfo == Tycho_EndlessRunner.GameState.gameover) return;
		TGMove ();    
		if (m_Transform [firstIndex].position.x <= transform.position.x+ xpos) {
			int next = firstIndex == 0 ? 1 : 0;
			m_Transform[firstIndex].position = m_Transform[next].position + Vector3.right * OffsetPosition;
			firstIndex = next;
		}
	}
}
