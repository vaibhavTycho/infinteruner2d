using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgMove_EndlessRunner : MonoBehaviour
{
    [SerializeField] private float offsetBack,OffsetFront,speed;
    private float screenWidth;
    [SerializeField] private List<Transform> Bg = new List<Transform>();
    private int firstIndex = 0;
    [SerializeField] private List<Vector2> startingpositions = new List<Vector2>();
    private void Start()
    {
        float worldScreenHeight = Camera.main.orthographicSize * 2;
        screenWidth = worldScreenHeight / Screen.height * Screen.width; ;
        for (int i = 0; i < Bg.Count; i++)
        {
            Bg[i].transform.position = new Vector2(Camera.main.transform.position.x + (i * screenWidth), transform.position.y);
            startingpositions.Add(Bg[i].transform.position);
        }
    }
    private void Update()
    {
        if(PlayerController_EndlessRunner.isPlayingNinja)
        {
            if(firstIndex>=Bg.Count)
                firstIndex =  firstIndex%Bg.Count;
            if ((Bg[firstIndex].position.x <= Camera.main.transform.position.x - (screenWidth + offsetBack)))
            {
                Bg[firstIndex].position = new Vector2(Camera.main.transform.position.x + (2 * screenWidth) - OffsetFront, transform.position.y);
                firstIndex++;
            }
                for(int i=0;i<Bg.Count;i++)
                {
                    Bg[i].Translate(Vector2.right * speed * Time.deltaTime);
                }
        }
     
    }
    public void ResetGame()
    {
        for (int i = 0; i < startingpositions.Count; i++)
        {
          //  Debug.Log("<color=red>"+i + " " + Bg[i].position + " " + startingpositions[i]+"</color>");
            Bg[i].position = startingpositions[i];
          //  Debug.Log(i+" "+Bg[i].position+" "+startingpositions[i]);
        }
        Bg[0].position = Vector2.zero;
        firstIndex = 0;
    }
}
