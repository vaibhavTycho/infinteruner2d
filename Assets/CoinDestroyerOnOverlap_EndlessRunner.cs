using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinDestroyerOnOverlap_EndlessRunner : MonoBehaviour
{
    public void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("KillBox"))
        {
            Debug.Log("Hit KillBox");
            // DestroyImmediate(this);
            var temp = gameObject.transform.position;
            transform.position = new Vector2(temp.x,temp.y+2);
        }
    }
}
