﻿using UnityEngine;
using System.Collections;

public class Tycho_EndlessRunner : MonoBehaviour {


	public enum GameState { title, get_ready, playing, gameover }

	public static GameState stateInfo = GameState.title;

	public static int score = 123456789;

	public virtual void Initialize() {}
}
